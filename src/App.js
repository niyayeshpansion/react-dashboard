import React from 'react'
import Topbar from './components/topbar/Topbar'
import SlideBar from "./components/slidebar/SlideBar";
import "./app.css";
import Home from "./pages/Home/Home";
function App() {
  return (
    <div>
      <h1>
        <Topbar />
          <div className={'container'}>
              <SlideBar />
              <Home />
              {/*<div className='content'>*/}
              {/*    content*/}
              {/*</div>*/}
          </div>
      </h1>
    </div>
  );
}

export default App;
