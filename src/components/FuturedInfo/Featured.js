import React from 'react';
import './Featured.css';
import {ArrowDownward, ArrowUpward} from "@material-ui/icons";
const Featured = () => {
    return (
        <div className={'featured'}>
            <div className="featuredItems">
                <span className="featuredTitle">Revenue</span>
                <div className="featuredMoneyContainer">
                    <span className="featuredMoney">$2,412</span>
                    <span className="featuredMoneyRate">
                        -12.4
                        <ArrowDownward  className="featuredIcon Negative"/>
                    </span>
                </div>
                <span className="featuredSub">
                    Compared To last month
                </span>
            </div>
            <div className="featuredItems">
                <span className="featuredTitle">Cost</span>
                <div className="featuredMoneyContainer">
                    <span className="featuredMoney">$2,412</span>
                    <span className="featuredMoneyRate">-
                        3.4
                        <ArrowDownward className="featuredIcon" />
                    </span>
                </div>
                <span className="featuredSub">
                    Compared To last month
                </span>
            </div>
            <div className="featuredItems">
                <span className="featuredTitle">Sales</span>
                <div className="featuredMoneyContainer">
                    <span className="featuredMoney">$2,412</span>
                    <span className="featuredMoneyRate">-
                        3.4
                        <ArrowUpward className="featuredIcon" />
                    </span>
                </div>
                <span className="featuredSub">
                    Compared To last month
                </span>
            </div>
        </div>
    );
};

export default Featured;