import React from 'react';
import { LineChart, Line, XAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

import "./Chart.css";


const Chart = ({title,data,dateKey,grid}) => {

    return (
        <div className='chart'>
           <h3 className="chartTitle"> {title} Analytics</h3>
            <ResponsiveContainer className={'chart-container'} width="100%" aspect={4 / 1}>
                <LineChart data={data}>
                    <XAxis dataKey="name" stroke={"#5550bd"} />
                    <Line type={"monotone"} stroke={"#5550bd"} dataKey={"Cash"} />
                    <Tooltip />
                    {grid && <CartesianGrid strokeDasharray={"5 5"}/>}
                    <Legend />
                </LineChart>
            </ResponsiveContainer>
        </div>
    );
};

export default Chart;