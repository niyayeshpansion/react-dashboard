import React from 'react';
import './SlideBar.css';
import {
    BarChart,
    Feedback, Group, Highlight,
    LineStyle,
    Mail, Message,
    Money,
    Person,
    Shop,
    Store,
    Timeline,
    TrendingUp, VerticalSplit
} from "@material-ui/icons";

const SlideBar = () => {
    return (
        <div className="SliderBar">
            <div className="slideBarWrapped">
                <div className="sliderBarMenu">
                    <h3 className="slideBarTitle">Dashboard</h3>
                    <ul className="slideBarList">
                        <li className="slideBarListItem active">
                            <LineStyle className='sliderBarIcon' />
                            Home
                        </li>
                        <li className="slideBarListItem">
                            <Timeline  className='sliderBarIcon'/>
                            Analytics
                        </li>
                        <li className="slideBarListItem">
                            <TrendingUp className='sliderBarIcon' />
                            Sale
                        </li>
                    </ul>
                </div>
                <div className="sliderBarMenu">
                    <h3 className="slideBarTitle">Quick menu</h3>
                    <ul className="slideBarList">
                        <li className="slideBarListItem ">
                            <Person className='sliderBarIcon' />
                            users
                        </li>
                        <li className="slideBarListItem">
                            <Store  className='sliderBarIcon'/>
                            Product
                        </li>
                        <li className="slideBarListItem">
                            <Money className='sliderBarIcon' />
                            Transaction
                        </li>
                        <li className="slideBarListItem">
                            <BarChart className='sliderBarIcon' />
                            Reports
                        </li>
                    </ul>
                </div>
                <div className="sliderBarMenu">
                    <h3 className="slideBarTitle">Notification </h3>
                    <ul className="slideBarList">
                        <li className="slideBarListItem ">
                            <Mail className='sliderBarIcon' />
                            Mail
                        </li>
                        <li className="slideBarListItem">
                            <Feedback  className='sliderBarIcon'/>
                            Feedback
                        </li>
                        <li className="slideBarListItem">
                            <Message className='sliderBarIcon' />
                            Message
                        </li>
                    </ul>
                </div>
                <div className="sliderBarMenu">
                    <h3 className="slideBarTitle">About</h3>
                    <ul className="slideBarList">
                        <li className="slideBarListItem ">
                            <Group className='sliderBarIcon' />
                            Company
                        </li>
                        <li className="slideBarListItem">
                            <Highlight  className='sliderBarIcon'/>
                            Team
                        </li>
                        <li className="slideBarListItem">
                            <VerticalSplit className='sliderBarIcon' />
                            Version 1.5.7
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default SlideBar;