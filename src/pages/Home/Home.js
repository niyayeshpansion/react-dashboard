import React from 'react';
import './Home.css'
import Featured from "../../components/FuturedInfo/Featured";
import Chart from "../../components/chart/Chart";
import {FinancialData} from "../../dummyData";

const Home = () => {
    return (
        <div className='home'>
            <Featured />
            <Chart data={FinancialData} title={'Financial'} dateKey="Financial status" grid   />
        </div>
    );
};

export default Home;